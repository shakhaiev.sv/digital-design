
const animationItems = document.querySelectorAll('.a-it');

if (animationItems.length > 0) {
    window.addEventListener('scroll', animationScrool);
    function animationScrool(params) {
        for (let index = 0; index < animationItems.length; index++) {
            const animItem = animationItems[index];
            const animItemHeight = animItem.offsetHeight;
            const animItemOffset = offset(animItem).top;
            const animStart = 4;

            let animItemPoint = window.innerHeight - animItemHeight / animStart;

            if (animItemHeight > window.innerHeight) {
                animItemPoint = window.innerHeight - window.innerHeight / animStart;
            }

            if ((pageYOffset > animItemOffset - animItemPoint)
                && pageYOffset < (animItemOffset + animItemHeight)) {
                animItem.classList.add('a-it--active')
            } else {
                if (!animItem.classList.contains('a-no-hidden')) {
                    animItem.classList.remove('a-it--active')
                }

            }

        }
    }
    setTimeout(() => {
        animationScrool()
    }, 300)


};

function offset(el) {
    const rect = el.getBoundingClientRect(),
        scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;

    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
};
